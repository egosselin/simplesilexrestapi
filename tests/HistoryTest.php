<?php
namespace Tests;

use Silex\WebTestCase;

class HistoryTest extends WebTestCase
{
    public function createApplication()
    {
		require __DIR__.'/../web/index.php';

		return $app;
    }
    
    public function testGetVerb()
    {
    	$client = $this->createClient(array('HTTP_HOST' => 'http://localhost:8080'));
    	
    	//Listing of all users
    	$client->request('GET', '/history/762AZ-12FGH-HJBL5-MP857');
    	$this->assertTrue($client->getResponse()->isSuccessful());
    	$this->assertContains('762AZ-12FGH-HJBL5-MP857', $client->getResponse()->getContent());
    }
    
    public function testPostVerb()
    {
    	$client = $this->createClient(array('HTTP_HOST' => 'http://localhost:8080'));
    	
    	//Listing of all users
    	$client->request('POST', '/history/762AZ-12FGH-HJBL5-MP857/JustTesting');
    	$this->assertTrue($client->getResponse()->isSuccessful());
    	$this->assertContains('Data for 762AZ-12FGH-HJBL5-MP857', $client->getResponse()->getContent());
    }
}