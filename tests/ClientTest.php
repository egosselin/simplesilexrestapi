<?php 
namespace Tests;

use Silex\WebTestCase;

class UserTest extends WebTestCase
{
    public function createApplication()
    {
		require __DIR__.'/../web/index.php';

		return $app;
    }
    
    public function testGetVerb()
    {
        $client = $this->createClient(array('HTTP_HOST' => 'http://localhost:8080'));

        //Listing of all users
        $client->request('GET', '/user/');
        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertContains('{"message":"2 users found"', $client->getResponse()->getContent());
        
        //Get data about one user
        $client->request('GET', '/user/762AZ-12FGH-HJBL5-MP857');
        $this->assertTrue($client->getResponse()->isSuccessful());
        $this->assertContains('{"message":"User 762AZ-12FGH-HJBL5-MP857"', $client->getResponse()->getContent());
        
        //Get data of an non-existing user
        $client->request('GET', '/user/00000-00000-00000-00000');
        $this->assertTrue($client->getResponse()->isNotFound());
    }
}