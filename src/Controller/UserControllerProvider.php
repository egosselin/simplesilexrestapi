<?php
namespace Api\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\DBAL\Query\QueryBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class UserControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        /**
         * Default GET handler
         *
         * List all users
         * usage : http://localhost:8080/user/
         */
        $controllers->get('/', function (Request $request) use ($app) {
            $selection = new QueryBuilder($app['db']);
            $selection
                ->select(
                    [
                        'cli.apikey',
                        'cli.lastname',
                        'cli.firstname',
                        'cli.address',
                        'cli.zipcode',
                        'cli.city'
                    ]
                )
                ->from('User', 'cli')
            ;

            $results = $selection->execute()->fetchAll();
            $output = [];

            if (empty($results)) {
                $output = ['message' => 'No user found', 'data' => []];
            } else {
                $output = ['message' => sprintf('%d users found', sizeof($results)), 'data' => $results];
            }

            return new JsonResponse($output);
        });

        
        /**
         * GET Handler for one user
         *
         * Get data about one user
         * usage : http://localhost:8080/user/{apikey}
         */
        $controllers->get('/{apikey}', function ($apikey) use ($app) {
            
            $selection = new QueryBuilder($app['db']);
            $selection
                ->select(
                    [
                        'cli.apikey',
                        'cli.lastname',
                        'cli.firstname',
                        'cli.address',
                        'cli.zipcode',
                        'cli.city'
                    ]
                )
                ->from('User', 'cli')
                ->where('cli.apikey = :key')
                ->setParameters(
                    [
                        'key' => $apikey
                    ]
                )
            ;
            $result = $selection->execute()->fetch();

            $output = [];
            
            if (empty($result)) {
                $app->abort(404, "User does not exist");
            } else {
                $output = array('message' => sprintf('User %s', $apikey), 'data' => $result);
                
                //A sub request to log the access
                $subPost = Request::create('/history/'.$apikey.'/ViewData', 'POST');
                $app->handle($subPost, HttpKernelInterface::SUB_REQUEST);
            }

            return new JsonResponse($output);
        })->assert('apikey', '[a-zA-Z0-9\-]+');

        return $controllers;
    }
}
