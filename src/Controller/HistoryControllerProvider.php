<?php

namespace Api\Controller;

use Doctrine\DBAL\Query\QueryBuilder;
use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class HistoryControllerProvider implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];
        
        /**
         * GET Handler
         * Return history about the selected user
         */
        $controllers->get('/{apikey}', function ($apikey) use ($app) {
        
            $selection = new QueryBuilder($app['db']);
            $selection
                ->select(
                    [
                        'his.apikey',
                        'his.action',
                        'his.created'
                    ]
                )
                ->from('History', 'his')
                ->where('his.apikey = :key')
                ->setParameters(['key' => $apikey])
            ;
            $results = $selection->execute()->fetch();
            
            $output = [];
            
            if ($results) {
                $output = ['results' => $results];
            } else {
                //If no results, returning an empty array
                $output = ['results' => array()];
            }
            
            return new JsonResponse($output);
        })->assert('apikey', '[a-zA-Z0-9\-]+')->assert('action', '[a-zA-Z]+');
        
        /**
         * POST Handler
         * Push data for the selected user
         */
        $controllers->post('/{apikey}/{action}', function ($apikey, $action) use ($app) {
            
            $insert = new QueryBuilder($app['db']);

            try {
                $insert->insert('History')
                    ->values(
                        [
                            'apikey' => sprintf('"%s"', $apikey),
                            'action' => sprintf('"%s"', $action),
                            'created' => sprintf('"%s"', date("Y-m-d H:i:s"))
                        ]
                    );
                $insert->execute();
            } catch (\Exception $e) {
                $app->abort(404, "Invalid query");
            }
            
            $reponse = array('message' => sprintf('Data for %s, pushed', $apikey));
            return new JsonResponse($reponse);
            
        })->assert('apikey', '[a-zA-Z0-9\-]+')->assert('action', '[a-zA-Z]+');
        
        return $controllers;
    }
}
