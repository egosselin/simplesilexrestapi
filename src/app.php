<?php

use Silex\Provider\DoctrineServiceProvider;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

$app->register(new DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver' => 'pdo_mysql',
        'dbhost' => 'localhost',
        'dbname' => 'apitest',
        'user' => 'root',
        'password' => 'root',
        'charset' => 'utf8'
    ),
));

//Sortie d'erreur
$app->error(function (\Exception $e, $code) use ($app) {
    
    if (!$app['debug']) {
        return new JsonResponse(array("Error" => $code));
    }
    
    $reponse = '<pre>Erreur '.$code.'</pre>';
    $reponse .= '<pre>'.$e->getMessage().'</pre>';
    $reponse .= "<pre>".$e->getTraceAsString()."</pre>";
    
    return new Response($reponse);
});

$app->get('/', function () use ($app) {
    return new Response(null);
});

//Chargement des controlleurs
$app->mount('/user', new Api\Controller\UserControllerProvider());
$app->mount('/history', new Api\Controller\HistoryControllerProvider());

return $app;
