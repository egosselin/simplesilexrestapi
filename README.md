Simple Silex REST API
===================

A simple example of structured REST API written in silex 

[![Build Status](https://api.travis-ci.org/egosselin1/simplesilexrestapi.svg)](https://travis-ci.org/egosselin1/simplesilexrestapi)

----------

Build a REST API quickly with this simple and elegant example.
In this project, i choosed to use controlers for a better code segmentation.

## Install ##

**Step 1**
Clone the repository

**Step 2**
Create the database structure

    CREATE DATABASE IF NOT EXISTS apitest DEFAULT CHARACTER SET utf8   COLLATE utf8_general_ci;
    USE apitest;

    CREATE TABLE IF NOT EXISTS `User` (
     `apikey` VARCHAR(45) NOT NULL,
     `firstname` VARCHAR(128) NOT NULL,
     `lastname` VARCHAR(128) NOT NULL,
     `address` VARCHAR(256) NOT NULL,
     `zipCode` VARCHAR(10) NOT NULL,
     `city` VARCHAR(45) NOT NULL,
     PRIMARY KEY (`apikey`),
     UNIQUE INDEX `apikey_UNIQUE` (`apikey` ASC))
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;

    CREATE TABLE IF NOT EXISTS `History` (
     `id` INT(11) NOT NULL AUTO_INCREMENT,
     `apikey` VARCHAR(45) NOT NULL,
     `action` VARCHAR(256) NOT NULL,
     `created` DATETIME NOT NULL,
     PRIMARY KEY (`id`),
     INDEX `fk_History_Client_idx` (`apikey` ASC),
     CONSTRAINT `fk_History_Client`
      FOREIGN KEY (`apikey`)
      REFERENCES apitest.`User` (`apikey`)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION)
    ENGINE = InnoDB
    DEFAULT CHARACTER SET = utf8;

    INSERT INTO `User` (`apikey`, `firstname`, `lastname`, `address`, `zipCode`, `city`) VALUES
    ('762AZ-12FGH-HJBL5-MP857', 'Alain', 'Deloin', '1 Avenue des Champs Elizees', '75016', 'Paris'),
    ('78YVE-897GH-32V79-TH6JB', 'George', 'Dupres', '12 Avenue Charle de Gaulle', '78510', 'Triel Sur Seine');

**Step 3**
Install composer dependencies

    composer install

**Step 4**
Launch a stand-alone php server from cli

    php -S localhost:8080 -t web web/index.php

## Test ##

 - List the users : http://localhost:8080/users/
 - Get one user : http://localhost:8080/users/762AZ-12FGH-HJBL5-MP857
 - Get history for the user above : http://localhost:8080/history/762AZ-12FGH-HJBL5-MP857
